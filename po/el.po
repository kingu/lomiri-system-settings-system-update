# Greek translation for lomiri-system-settings-system-update
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the lomiri-system-settings-system-update package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-system-update\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-14 15:50+0000\n"
"PO-Revision-Date: 2020-10-29 01:02+0000\n"
"Last-Translator: George Kitsopoulos <gkitsopoulos@tutanota.com>\n"
"Language-Team: Greek <https://translate.ubports.com/projects/ubports/system-"
"settings/el/>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2015-07-16 05:40+0000\n"

#: ../plugins/system-update/ChangelogExpander.qml:43
#, qt-format
msgid "Version %1"
msgstr "Έκδοση %1"

#: ../plugins/system-update/ChannelSettings.qml:33
msgid "Channel settings"
msgstr "Ρυθμίσεις καναλιού"

#: ../plugins/system-update/ChannelSettings.qml:58
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Development"
msgstr "Ανάπτυξη"

#: ../plugins/system-update/ChannelSettings.qml:58
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Release candidate"
msgstr "Υποψήφια έκδοση"

#: ../plugins/system-update/ChannelSettings.qml:58
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Stable"
msgstr "Σταθερή"

#: ../plugins/system-update/ChannelSettings.qml:95
msgid "Fetching channels"
msgstr "Λήψη καναλιών"

#: ../plugins/system-update/ChannelSettings.qml:104
msgid "Channel to get updates from:"
msgstr "Κανάλι για λήψη ενημερώσεων:"

#: ../plugins/system-update/ChannelSettings.qml:120
msgid "Switching channel"
msgstr "Εναλλαγή καναλιού"

#: ../plugins/system-update/Configuration.qml:30
#: ../plugins/system-update/UpdateSettings.qml:55
msgid "Auto download"
msgstr "Αυτόματη λήψη"

#: ../plugins/system-update/Configuration.qml:36
msgid "Download future updates automatically:"
msgstr "Λήψη νέων ενημερώσεων αυτόματα:"

#: ../plugins/system-update/Configuration.qml:54
#: ../plugins/system-update/UpdateSettings.qml:58
msgid "Never"
msgstr "Ποτέ"

#: ../plugins/system-update/Configuration.qml:55
msgid "When on WiFi"
msgstr "Όταν σε σύνδεση ασύρματου δικτύου"

#: ../plugins/system-update/Configuration.qml:57
msgid "On any data connection"
msgstr "Σε οποιαδήποτε σύνδεση δεδομένων"

#: ../plugins/system-update/Configuration.qml:58
msgid "Data charges may apply."
msgstr "Μπορεί να υπάρχουν χρεώσεις δεδομένων."

#: ../plugins/system-update/DownloadHandler.qml:175
#: ../plugins/system-update/InstallationFailed.qml:26
msgid "Installation failed"
msgstr "Η εγκατάσταση απέτυχε"

#: ../plugins/system-update/FirmwareUpdate.qml:37
#: ../plugins/system-update/FirmwareUpdate.qml:158
msgid "Firmware Update"
msgstr "Ενημέρωση υλικολογισμικού"

#: ../plugins/system-update/FirmwareUpdate.qml:134
msgid "There is a firmware update available!"
msgstr "Είναι διαθέσιμη ενημέρωση υλικολογισμικού!"

#: ../plugins/system-update/FirmwareUpdate.qml:135
msgid "Firmware is up to date!"
msgstr "Το υλικολογισμικό είναι ενημερωμένο!"

#: ../plugins/system-update/FirmwareUpdate.qml:170
msgid "The device will restart automatically after installing is done."
msgstr ""
"Η συσκευή θα επανεκκινηθεί αυτόματα μετά την ολοκλήρωση της εγκατάστασης."

#: ../plugins/system-update/FirmwareUpdate.qml:186
msgid "Install and restart now"
msgstr "Εγκατάσταση & επανεκκίνηση τώρα"

#: ../plugins/system-update/FirmwareUpdate.qml:229
msgid ""
"Downloading and Flashing firmware updates, this could take a few minutes..."
msgstr ""
"Λήψη και αναβάθμιση ενημερώσεων υλικολογισμικού, αυτό μπορεί να διαρκέσει "
"λίγα λεπτά..."

#: ../plugins/system-update/FirmwareUpdate.qml:236
msgid "Checking for firmware update"
msgstr "Έλεγχος για ενημερώσεις υλικολογισμικού"

#: ../plugins/system-update/GlobalUpdateControls.qml:85
msgid "Checking for updates…"
msgstr "Έλεγχος για ενημερώσεις…"

#: ../plugins/system-update/GlobalUpdateControls.qml:90
msgid "Stop"
msgstr "Διακοπή"

#. TRANSLATORS: %1 is number of software updates available.
#: ../plugins/system-update/GlobalUpdateControls.qml:117
#, qt-format
msgid "%1 update available"
msgid_plural "%1 updates available"
msgstr[0] "%1 διαθέσιμη ενημέρωση"
msgstr[1] "%1 διαθέσιμες ενημερώσεις"

#: ../plugins/system-update/GlobalUpdateControls.qml:127
msgid "Update all…"
msgstr "Ενημέρωση όλων…"

#: ../plugins/system-update/GlobalUpdateControls.qml:129
msgid "Update all"
msgstr "Ενημέρωση όλων"

#: ../plugins/system-update/ImageUpdatePrompt.qml:31
msgid "Update System"
msgstr "Ενημέρωση συστήματος"

#: ../plugins/system-update/ImageUpdatePrompt.qml:33
msgid "The device needs to restart to install the system update."
msgstr ""
"Η συσκευή πρέπει να επανεκκινηθεί για να εγκαταστήσει την ενημέρωση "
"συστήματος."

#: ../plugins/system-update/ImageUpdatePrompt.qml:34
msgid "Connect the device to power before installing the system update."
msgstr ""
"Συνδέστε τη συσκευή στην τροφοδοσία πριν εγκαταστήσετε την ενημέρωση "
"συστήματος."

#: ../plugins/system-update/ImageUpdatePrompt.qml:39
msgid "Restart & Install"
msgstr "Επανεκκίνηση & εγκατάσταση"

#: ../plugins/system-update/ImageUpdatePrompt.qml:49
#: ../plugins/system-update/PageComponent.qml:125
#: ../plugins/system-update/PageComponent.qml:126
msgid "Cancel"
msgstr "Ακύρωση"

#: ../plugins/system-update/InstallationFailed.qml:29
msgid "OK"
msgstr "ΟΚ"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../plugins/system-update/PageComponent.qml:38 settings.js:2
msgid "Updates"
msgstr "Ενημερώσεις"

#: ../plugins/system-update/PageComponent.qml:44
#: ../plugins/system-update/UpdateSettings.qml:32
msgid "Update settings"
msgstr "Ενημέρωση ρυθμίσεων"

#: ../plugins/system-update/PageComponent.qml:52
#: ../plugins/system-update/PageComponent.qml:111
#, fuzzy
#| msgid "Recent updates"
msgid "Clear updates"
msgstr "Πρόσφατες ενημερώσεις"

#: ../plugins/system-update/PageComponent.qml:112
msgid "Are you sure you want to clear the update list?"
msgstr ""

#: ../plugins/system-update/PageComponent.qml:129
msgid "Clear"
msgstr ""

#: ../plugins/system-update/PageComponent.qml:175
#: ../plugins/system-update/PageComponent.qml:223
#: ../plugins/system-update/PageComponent.qml:224
#: ../plugins/system-update/ReinstallAllApps.qml:150
msgid "Connect to the Internet to check for updates."
msgstr "Συνδεθείτε στο Ίντερνετ για να γίνει έλεγχος ενημερώσεων."

#: ../plugins/system-update/PageComponent.qml:177
#: ../plugins/system-update/PageComponent.qml:225
#: ../plugins/system-update/PageComponent.qml:226
#: ../plugins/system-update/ReinstallAllApps.qml:152
msgid "Software is up to date"
msgstr "Το λογισμικό είναι ενημερωμένο"

#: ../plugins/system-update/PageComponent.qml:180
#: ../plugins/system-update/PageComponent.qml:228
#: ../plugins/system-update/PageComponent.qml:229
#: ../plugins/system-update/ReinstallAllApps.qml:155
msgid "The update server is not responding. Try again later."
msgstr "Ο διακομιστής ενημερώσεων δεν αποκρίνεται. Προσπαθήστε ξανά αργότερα."

#: ../plugins/system-update/PageComponent.qml:189
#: ../plugins/system-update/PageComponent.qml:237
#: ../plugins/system-update/PageComponent.qml:238
#: ../plugins/system-update/ReinstallAllApps.qml:164
msgid "Updates Available"
msgstr "Διαθέσιμες ενημερώσεις"

#: ../plugins/system-update/PageComponent.qml:323
#: ../plugins/system-update/PageComponent.qml:371
#: ../plugins/system-update/PageComponent.qml:372
msgid "Recent updates"
msgstr "Πρόσφατες ενημερώσεις"

#: ../plugins/system-update/ReinstallAllApps.qml:37
#: ../plugins/system-update/ReinstallAllApps.qml:96
#: ../plugins/system-update/UpdateSettings.qml:88
msgid "Reinstall all apps"
msgstr "Επανεγκατάσταση όλων των εφαρμογών"

#: ../plugins/system-update/ReinstallAllApps.qml:86
msgid ""
"Use this to get all of the latest apps, typically needed after a major "
"system upgrade, "
msgstr ""
"Χρησιμοποιήστε αυτό για να λάβετε όλες τις πιο πρόσφατες εφαρμογές, που "
"συνήθως χρειάζονται μετά από μια σημαντική αναβάθμιση του συστήματος, "

#: ../plugins/system-update/UpdateDelegate.qml:120
msgid "Retry"
msgstr "Προσπάθεια ξανά"

#: ../plugins/system-update/UpdateDelegate.qml:125
msgid "Update"
msgstr "Ενημέρωση"

#: ../plugins/system-update/UpdateDelegate.qml:127
msgid "Download"
msgstr "Λήψη"

#: ../plugins/system-update/UpdateDelegate.qml:133
msgid "Resume"
msgstr "Συνέχιση"

#: ../plugins/system-update/UpdateDelegate.qml:140
msgid "Pause"
msgstr "Παύση"

#: ../plugins/system-update/UpdateDelegate.qml:144
msgid "Install…"
msgstr "Γίνεται εγκατάσταση…"

#: ../plugins/system-update/UpdateDelegate.qml:146
msgid "Install"
msgstr "Εγκατάσταση"

#: ../plugins/system-update/UpdateDelegate.qml:150
msgid "Open"
msgstr "Άνοιγμα"

#: ../plugins/system-update/UpdateDelegate.qml:229
msgid "Installing"
msgstr "Γίνεται εγκατάσταση"

#: ../plugins/system-update/UpdateDelegate.qml:233
msgid "Paused"
msgstr "Σε παύση"

#: ../plugins/system-update/UpdateDelegate.qml:236
msgid "Waiting to download"
msgstr "Αναμονή για λήψη"

#: ../plugins/system-update/UpdateDelegate.qml:239
msgid "Downloading"
msgstr "Λήψη σε εξέλιξη"

#. TRANSLATORS: %1 is the human readable amount
#. of bytes downloaded, and %2 is the total to be
#. downloaded.
#: ../plugins/system-update/UpdateDelegate.qml:275
#, qt-format
msgid "%1 of %2"
msgstr "%1 από %2"

#: ../plugins/system-update/UpdateDelegate.qml:279
msgid "Downloaded"
msgstr "Λήψεις"

#: ../plugins/system-update/UpdateDelegate.qml:282
msgid "Installed"
msgstr "Εγκατεστημένα"

#. TRANSLATORS: %1 is the date at which this
#. update was applied.
#: ../plugins/system-update/UpdateDelegate.qml:287
#, qt-format
msgid "Updated %1"
msgstr "Ενημερώθηκε την %1"

#: ../plugins/system-update/UpdateDelegate.qml:311
msgid "Update failed"
msgstr "Η ενημέρωση απέτυχε"

#: ../plugins/system-update/UpdateSettings.qml:60
msgid "On WiFi"
msgstr "Σε ασύρματο δίκτυο"

#: ../plugins/system-update/UpdateSettings.qml:62
msgid "Always"
msgstr "Πάντα"

#: ../plugins/system-update/UpdateSettings.qml:64
msgid "Unknown"
msgstr "Άγνωστο"

#: ../plugins/system-update/UpdateSettings.qml:70
msgid "Channels"
msgstr "Κανάλια"

#: ../push-helper/software_updates_helper.py:159
#: ../push-helper/software_updates_helper.py:160
msgid "There's an updated system image."
msgstr ""

#: ../push-helper/software_updates_helper.py:160
#: ../push-helper/software_updates_helper.py:161
msgid "Tap to open the system updater."
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:4
msgid "system"
msgstr "σύστημα"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:6
msgid "software"
msgstr "λογισμικό"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:8
msgid "update"
msgstr "ενημέρωση"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:10
msgid "apps"
msgstr "εφαρμογές"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:12
msgid "application"
msgstr "εφαρμογή"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:14
msgid "automatic"
msgstr "αυτόματα"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:16
msgid "download"
msgstr "λήψη"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:18
msgid "upgrade"
msgstr "αναβάθμιση"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:20
msgid "click"
msgstr "click"

#. TRANSLATORS: This is a keyword or name for the update-notification plugin which is used while searching
#: settings.js:22
#, fuzzy
#| msgid "Updates Available"
msgid "Updates available"
msgstr "Διαθέσιμες ενημερώσεις"
