# Galician translation for lomiri-system-settings-system-update
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the lomiri-system-settings-system-update package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-system-update\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-14 15:50+0000\n"
"PO-Revision-Date: 2020-09-15 17:43+0000\n"
"Last-Translator: Neutrum N <neutro@tutanota.com>\n"
"Language-Team: Galician <https://translate.ubports.com/projects/ubports/"
"system-settings/gl/>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2015-07-16 05:40+0000\n"

#: ../plugins/system-update/ChangelogExpander.qml:43
#, qt-format
msgid "Version %1"
msgstr "Versión %1"

#: ../plugins/system-update/ChannelSettings.qml:33
msgid "Channel settings"
msgstr "Axustes da canle"

#: ../plugins/system-update/ChannelSettings.qml:58
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Development"
msgstr "Desenvolvemento"

#: ../plugins/system-update/ChannelSettings.qml:58
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Release candidate"
msgstr "Versión candidata"

#: ../plugins/system-update/ChannelSettings.qml:58
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Stable"
msgstr "Estábel"

#: ../plugins/system-update/ChannelSettings.qml:95
msgid "Fetching channels"
msgstr "Obtendo canles"

#: ../plugins/system-update/ChannelSettings.qml:104
msgid "Channel to get updates from:"
msgstr "Canle da cal obter actualizacións:"

#: ../plugins/system-update/ChannelSettings.qml:120
msgid "Switching channel"
msgstr "Cambiando a canle"

#: ../plugins/system-update/Configuration.qml:30
#: ../plugins/system-update/UpdateSettings.qml:55
msgid "Auto download"
msgstr "Descarga automática"

#: ../plugins/system-update/Configuration.qml:36
msgid "Download future updates automatically:"
msgstr "Descargar actualizacións futuras automaticamente:"

#: ../plugins/system-update/Configuration.qml:54
#: ../plugins/system-update/UpdateSettings.qml:58
msgid "Never"
msgstr "Nunca"

#: ../plugins/system-update/Configuration.qml:55
msgid "When on WiFi"
msgstr "Só en modo wifi"

#: ../plugins/system-update/Configuration.qml:57
msgid "On any data connection"
msgstr "Con calquera conexión de datos"

#: ../plugins/system-update/Configuration.qml:58
msgid "Data charges may apply."
msgstr "Pódese aplicar a tarifa de datos."

#: ../plugins/system-update/DownloadHandler.qml:175
#: ../plugins/system-update/InstallationFailed.qml:26
msgid "Installation failed"
msgstr "A instalación fallou"

#: ../plugins/system-update/FirmwareUpdate.qml:37
#: ../plugins/system-update/FirmwareUpdate.qml:158
msgid "Firmware Update"
msgstr "Actualización de firmware"

#: ../plugins/system-update/FirmwareUpdate.qml:134
msgid "There is a firmware update available!"
msgstr "Actualización de firmware dispoñíbel!"

#: ../plugins/system-update/FirmwareUpdate.qml:135
msgid "Firmware is up to date!"
msgstr "O firmware está actualizado!"

#: ../plugins/system-update/FirmwareUpdate.qml:170
msgid "The device will restart automatically after installing is done."
msgstr ""
"O dispositivo reiniciarase automaticamente despois de rematar a instalación."

#: ../plugins/system-update/FirmwareUpdate.qml:186
msgid "Install and restart now"
msgstr "Instalar e reiniciar agora"

#: ../plugins/system-update/FirmwareUpdate.qml:229
msgid ""
"Downloading and Flashing firmware updates, this could take a few minutes..."
msgstr ""
"Descargando e gravando actualizacións de firmware, esto podería levar uns "
"minutos..."

#: ../plugins/system-update/FirmwareUpdate.qml:236
msgid "Checking for firmware update"
msgstr "Buscando actualización de firmware"

#: ../plugins/system-update/GlobalUpdateControls.qml:85
msgid "Checking for updates…"
msgstr "Buscando actualizacións…"

#: ../plugins/system-update/GlobalUpdateControls.qml:90
msgid "Stop"
msgstr "Parar"

#. TRANSLATORS: %1 is number of software updates available.
#: ../plugins/system-update/GlobalUpdateControls.qml:117
#, qt-format
msgid "%1 update available"
msgid_plural "%1 updates available"
msgstr[0] "%1 actualización dispoñíbel"
msgstr[1] "%1 actualizacións dispoñíbeis"

#: ../plugins/system-update/GlobalUpdateControls.qml:127
msgid "Update all…"
msgstr "Actualizar todo…"

#: ../plugins/system-update/GlobalUpdateControls.qml:129
msgid "Update all"
msgstr "Actualizar todo"

#: ../plugins/system-update/ImageUpdatePrompt.qml:31
msgid "Update System"
msgstr "Actualizar o sistema"

#: ../plugins/system-update/ImageUpdatePrompt.qml:33
msgid "The device needs to restart to install the system update."
msgstr ""
"O dispositivo precisa reiniciarse para instalar a actualización do sistema."

#: ../plugins/system-update/ImageUpdatePrompt.qml:34
msgid "Connect the device to power before installing the system update."
msgstr ""
"Conectar o dispositivo á corrente antes de instalar a actualización do "
"sistema."

#: ../plugins/system-update/ImageUpdatePrompt.qml:39
msgid "Restart & Install"
msgstr "Reiniciar e instalar"

#: ../plugins/system-update/ImageUpdatePrompt.qml:49
#: ../plugins/system-update/PageComponent.qml:125
#: ../plugins/system-update/PageComponent.qml:126
msgid "Cancel"
msgstr "Cancelar"

#: ../plugins/system-update/InstallationFailed.qml:29
msgid "OK"
msgstr "Aceptar"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../plugins/system-update/PageComponent.qml:38 settings.js:2
msgid "Updates"
msgstr "Actualizacións"

#: ../plugins/system-update/PageComponent.qml:44
#: ../plugins/system-update/UpdateSettings.qml:32
msgid "Update settings"
msgstr "Axustes de actualizacións"

#: ../plugins/system-update/PageComponent.qml:52
#: ../plugins/system-update/PageComponent.qml:111
#, fuzzy
#| msgid "Recent updates"
msgid "Clear updates"
msgstr "Actualizacións recentes"

#: ../plugins/system-update/PageComponent.qml:112
msgid "Are you sure you want to clear the update list?"
msgstr ""

#: ../plugins/system-update/PageComponent.qml:129
msgid "Clear"
msgstr ""

#: ../plugins/system-update/PageComponent.qml:175
#: ../plugins/system-update/PageComponent.qml:223
#: ../plugins/system-update/PageComponent.qml:224
#: ../plugins/system-update/ReinstallAllApps.qml:150
msgid "Connect to the Internet to check for updates."
msgstr "Conectar a Internet para buscar actualizacións."

#: ../plugins/system-update/PageComponent.qml:177
#: ../plugins/system-update/PageComponent.qml:225
#: ../plugins/system-update/PageComponent.qml:226
#: ../plugins/system-update/ReinstallAllApps.qml:152
msgid "Software is up to date"
msgstr "O software está actualizado"

#: ../plugins/system-update/PageComponent.qml:180
#: ../plugins/system-update/PageComponent.qml:228
#: ../plugins/system-update/PageComponent.qml:229
#: ../plugins/system-update/ReinstallAllApps.qml:155
msgid "The update server is not responding. Try again later."
msgstr "O servidor de actualizacións non responde. Ténteo máis tarde."

#: ../plugins/system-update/PageComponent.qml:189
#: ../plugins/system-update/PageComponent.qml:237
#: ../plugins/system-update/PageComponent.qml:238
#: ../plugins/system-update/ReinstallAllApps.qml:164
msgid "Updates Available"
msgstr "Actualizacións dispoñíbeis"

#: ../plugins/system-update/PageComponent.qml:323
#: ../plugins/system-update/PageComponent.qml:371
#: ../plugins/system-update/PageComponent.qml:372
msgid "Recent updates"
msgstr "Actualizacións recentes"

#: ../plugins/system-update/ReinstallAllApps.qml:37
#: ../plugins/system-update/ReinstallAllApps.qml:96
#: ../plugins/system-update/UpdateSettings.qml:88
msgid "Reinstall all apps"
msgstr "Reinstalar todos os aplicativos"

#: ../plugins/system-update/ReinstallAllApps.qml:86
msgid ""
"Use this to get all of the latest apps, typically needed after a major "
"system upgrade, "
msgstr ""
"Use esto para obter os aplicativos máis recentes, precísase tipicamente "
"despois dunha actualización do sistema importante, "

#: ../plugins/system-update/UpdateDelegate.qml:120
msgid "Retry"
msgstr "Reintentar"

#: ../plugins/system-update/UpdateDelegate.qml:125
msgid "Update"
msgstr "Actualizar"

#: ../plugins/system-update/UpdateDelegate.qml:127
msgid "Download"
msgstr "Descargar"

#: ../plugins/system-update/UpdateDelegate.qml:133
msgid "Resume"
msgstr "Continuar"

#: ../plugins/system-update/UpdateDelegate.qml:140
msgid "Pause"
msgstr "Pausar"

#: ../plugins/system-update/UpdateDelegate.qml:144
msgid "Install…"
msgstr "Instalar…"

#: ../plugins/system-update/UpdateDelegate.qml:146
msgid "Install"
msgstr "Instalar"

#: ../plugins/system-update/UpdateDelegate.qml:150
msgid "Open"
msgstr "Abrir"

#: ../plugins/system-update/UpdateDelegate.qml:229
msgid "Installing"
msgstr "Instalando"

#: ../plugins/system-update/UpdateDelegate.qml:233
msgid "Paused"
msgstr "En pausa"

#: ../plugins/system-update/UpdateDelegate.qml:236
msgid "Waiting to download"
msgstr "Esperando para descargar"

#: ../plugins/system-update/UpdateDelegate.qml:239
msgid "Downloading"
msgstr "Descargando"

#. TRANSLATORS: %1 is the human readable amount
#. of bytes downloaded, and %2 is the total to be
#. downloaded.
#: ../plugins/system-update/UpdateDelegate.qml:275
#, qt-format
msgid "%1 of %2"
msgstr "%1 de %2"

#: ../plugins/system-update/UpdateDelegate.qml:279
msgid "Downloaded"
msgstr "Descargado"

#: ../plugins/system-update/UpdateDelegate.qml:282
msgid "Installed"
msgstr "Instalado"

#. TRANSLATORS: %1 is the date at which this
#. update was applied.
#: ../plugins/system-update/UpdateDelegate.qml:287
#, qt-format
msgid "Updated %1"
msgstr "Actualizado o %1"

#: ../plugins/system-update/UpdateDelegate.qml:311
msgid "Update failed"
msgstr "A actualización fallou"

#: ../plugins/system-update/UpdateSettings.qml:60
msgid "On WiFi"
msgstr "Con wifi"

#: ../plugins/system-update/UpdateSettings.qml:62
msgid "Always"
msgstr "Sempre"

#: ../plugins/system-update/UpdateSettings.qml:64
msgid "Unknown"
msgstr "Descoñecido"

#: ../plugins/system-update/UpdateSettings.qml:70
msgid "Channels"
msgstr "Canles"

#: ../push-helper/software_updates_helper.py:159
#: ../push-helper/software_updates_helper.py:160
msgid "There's an updated system image."
msgstr ""

#: ../push-helper/software_updates_helper.py:160
#: ../push-helper/software_updates_helper.py:161
msgid "Tap to open the system updater."
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:4
msgid "system"
msgstr "sistema"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:6
msgid "software"
msgstr "software"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:8
msgid "update"
msgstr "actualizar"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:10
msgid "apps"
msgstr "aplicativos"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:12
msgid "application"
msgstr "aplicativo"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:14
msgid "automatic"
msgstr "automático"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:16
msgid "download"
msgstr "descargar"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:18
msgid "upgrade"
msgstr "actualizar"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:20
msgid "click"
msgstr "click"

#. TRANSLATORS: This is a keyword or name for the update-notification plugin which is used while searching
#: settings.js:22
#, fuzzy
#| msgid "Updates Available"
msgid "Updates available"
msgstr "Actualizacións dispoñíbeis"
